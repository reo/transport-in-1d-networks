import Convergence_Convection_Bifurcation as Conv_Comp
import numpy as np
import scipy as sp
#import progressbar

def Main(nbNodes, edges, nodeTypes, radius, spaceStep, OutFlow, flow, Pressure, typeOfSolver, typeOfInput, typeOfSL, time , cBlood_PP, time_PP, cflDeltaT):
    Model = Conv_Comp.Convection_computation(nbNodes, edges, nodeTypes, radius, spaceStep, OutFlow, flow, Pressure, typeOfSolver, typeOfInput, typeOfSL, time[-1], cflDeltaT, cBlood_PP, time_PP)
    U     = np.zeros(Model.nbSpaceSteps)
    #print (Model.typeOfInput)
    state = np.zeros((Model.nbSpaceSteps,1))

    for i in range(Model.nbSpaceSteps):
      state[i,0] = U[i]
    
    #bar=progressbar.ProgressBar(max_value=len(time)-1)

    for n in range(len(time)-1):
      X = state[:,-1].reshape(Model.nbSpaceSteps)
      X = Model.PropageOneTimePoint(X, time[n], time[n+1])
      newX = np.zeros((Model.nbSpaceSteps,1))
      for i in range(len(X)):
          newX[i,0] = X[i]
      state = sp.hstack((state, newX))
      #bar.update(n)


    insideAtEnd = 0
    for i in range(Model.nbSpaceSteps):
        insideAtEnd = insideAtEnd + state[i,-1]


    print ('Global ', 'Error to CFL=1: ', max(Model.errorREL),'In: ', Model.insideConcentration, 'Out: ', Model.outsideConcentration, 'End: ', insideAtEnd, 'Delta: ', Model.insideConcentration - (Model.outsideConcentration + insideAtEnd))

    return state, Model.nbSpaceSteps, time, Model.Volume
