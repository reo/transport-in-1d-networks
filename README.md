# transport in 1D networks

code sharing for a mass-conservative scheme for transport (convection-reaction) in a network of 1D pipes. Publication: Boissier, Drasdo and Vignon-Clementel, IJNMBE, accepted in 2020. 
[Thank you to Lorenzo Sala for making the code run on python3!]

The code runs for a bifurcation by setting in Sim_init if you want to run a joining (converging) or splitting (diverging) bifurcation. The new scheme is useful for the converging bifurcation. The figure outputs that correspond to fig.6 of the manuscript are in the repository.

To run the code, you need to install python3 with scipy, numpy, matplotlib and type: python run-ErrorToExactSolution_Bif_11_2020.py

