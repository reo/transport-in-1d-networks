#%%
'''
Geometry and flow initialization

Provides two settings for a simple bifurcation - splitting and joining. For each setting, node and edge labels are created along with flow-related parameters along the domain: radius (uniform), velocity field (uniform with kirchhoff-like condition at bifurcation), pressure field (linear).
'''
import scipy as sp
import matplotlib.pyplot as plt
import numpy as np
# import matplotlib.pyplot as plt


#######
def sim_initSplitting(n, n2, length):
    '''Initialization for splitting bifurcation.'''
    
    # Create nodes 
    nbNodes = (int)((2*n2+n)*length+1)
    print(nbNodes)
    print(n*16) # TODO: Why is this here?
    
    # Create edges
    edges = np.zeros((nbNodes-1,2), dtype=int)
    
    # Edges before bifurcation
    for i in range((int)(n*length)):
        edges[i][0] = i
        edges[i][1] = i+1
    
    # Edges after bifurcation (1st branch) TODO: Why like this?
    for i in range((int)(n*length), (int)((n+n2)*length-1)):
        edges[i][0] = i+1
        edges[i][1] = i+2
    
    # Edge between last node of 1st branch and intersection node TODO: Why?
    edges[(int)((n+n2)*length-1)][0] = (int)((n+n2)*length)
    edges[(int)((n+n2)*length-1)][1] = (int)(n*length)

    # 1st edge of 2nd branch
    edges[(int)((n+n2)*length)][0] = (int)(n*length)
    edges[(int)((n+n2)*length)][1] = (int)((n+n2)*length+1)

    # 2nd branch
    for i in range((int)((n+n2)*length+1), (int)((2*n2+n)*length)):
        edges[i][0] = i
        edges[i][1] = i+1

    # Vessel radius (uniform)
    radius = 3.61*np.ones(nbNodes)

    # Node marking
    nodeTypes = np.zeros(nbNodes) # default node marking is 0
    nodeTypes[0] = 2 # inlet node
    nodeTypes[(int)((2*n2+n)*length)] = 1 # outlet node
    nodeTypes[(int)(n*length+1)] = 1 # outlet node

    # Flow along edges (uniform with Kirchhoff-like law at intersection)
    # TODO: Using `radius[i]` could be dangerous bc radius is defined node-wise. I suspect that if radii weren't uniform, this approach wouldn't work!
    Flow = np.zeros(len(edges))
    for i in range((int)(n*length)):
        Flow[i] = 80.0*sp.pi*radius[i]**2
    for i in range((int)(n*length), len(edges)):
        Flow[i] = 80.0/2.*sp.pi*radius[i]**2

    # Outflow at nodes
    OutFlow  = np.zeros(nbNodes)
    for i in range((int)(n*length+1)):
        OutFlow[i] = 80.0*sp.pi*radius[i]**2
    for i in range((int)(n*length+1),nbNodes):
        OutFlow[i] = 80.0/2.*sp.pi*radius[i]**2
    
    # Pressure at nodes (linear)
    Pressure = np.zeros(nbNodes)
    for i in range((int)(n*length+1)):
        Pressure[i] = 200*(n*length - i)/(n*length) + 100
    for i in range((int)(n*length+1), (int)((n+n2)*length+1)):
        Pressure[i] = -(n*length-i)*(1./((n2)*length))*100
    for i in range((int)((n+n2)*length+1), nbNodes):
        Pressure[i] = ((n2*2+n)*length+1 - i)*(1./((n2)*length))*100
        
        
    return nbNodes, edges, nodeTypes, radius, OutFlow, Flow, Pressure

#########

def sim_initJoining(n, n2, length):
    '''Initialization for joining bifurcation.'''
    
    # Create nodes TODO: No print here?
    nbNodes = (int)((2*n2+n)*length+1)
    # Create edges
    edges = np.zeros((nbNodes-1,2),dtype=int)
    
    # Edges before bifurcation
    for i in range((int)(n*length)):
        edges[i][0] = i
        edges[i][1] = i+1
    
    # Edges after bifurcation (1st branch)
    for i in range((int)(n*length), (int)((n+n2)*length-1)):
        edges[i][0] = i+1
        edges[i][1] = i+2
    
    # Edge between last node of 1st branch and intersection node
    edges[(int)((n+n2)*length-1)][0] = (int)((n+n2)*length)
    edges[(int)((n+n2)*length-1)][1] = (int)(n*length)
    
    # 1st edge of 2nd branch
    edges[(int)((n+n2)*length)][0] = (int)(n*length)
    edges[(int)((n+n2)*length)][1] = (int)((n+n2)*length+1)

    # 2nd branch
    for i in range((int)((n+n2)*length+1), (int)((2*n2+n)*length)):
        edges[i][0] = i
        edges[i][1] = i+1

    # Vessel radius (uniform)
    radius         = 3.61*np.ones(nbNodes)

    # Node marking
    nodeTypes = np.zeros(nbNodes,dtype=int) # default node marking is 0
    nodeTypes[0] = 1 # outlet node
    nodeTypes[(int)((2*n2+n)*length)] = 2 # inlet node
    nodeTypes[(int)(n*length+1)] = 2 # inlet node

    # Flow along edges (uniform with Kirchhoff-like law at intersection)
    Flow = np.zeros(len(edges))
    for i in range((int)(n*length)):
        Flow[i] = 80.0*sp.pi*radius[i]**2
    for i in range((int)(n*length), len(edges)):
        Flow[i] = 80.0/2.*sp.pi*radius[i]**2

    # Outflow at nodes
    OutFlow  = np.zeros(nbNodes)
    for i in range((int)(n*length+1)):
        OutFlow[i] = 80.0*sp.pi*radius[i]**2
    for i in range((int)(n*length+1),nbNodes):
        OutFlow[i] = 80.0/2.*sp.pi*radius[i]**2
    
    # Pressure at nodes (linear)
    Pressure = np.zeros(nbNodes)
    for i in range((int)(n*length+1)):
        Pressure[i] = 100-(n*length +1 - i)/(n*length+1)*100
    for i in range((int)(n*length+1), (int)((n+n2)*length+1)):
        Pressure[i] = 200*((n+n2)*length-i)*(1./((n+n2)*length))+102
    for i in range((int)((n+n2)*length+1), nbNodes):
        Pressure[i] = -200*((n+n2)*length+1 - i)/((n+n2)*length+1) + 102
        
    return nbNodes, edges, nodeTypes, radius, OutFlow, Flow, Pressure

if __name__ == '__main__':
    nbNodes, edges, nodeTypes, _, _, Flow, Pressure = sim_initSplitting(500, 500, 1)
    # print(edges)
    # print(Flow)
    # print(Pressure)
    plt.plot(Flow)
    plt.show()
    plt.plot(Pressure)
    plt.show()
    nbNodes, edges, nodeTypes, _, _, Flow, Pressure = sim_initJoining(500, 500, 1)
    # print(edges)
    # print(Flow)
    # print(Pressure)
    plt.plot(Flow)
    plt.show()
    plt.plot(Pressure)
    plt.show()
# %%
