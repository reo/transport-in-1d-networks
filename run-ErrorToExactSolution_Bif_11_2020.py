from Sim_init import *
from Main_bifurcation import *

## Common parameters
cflDeltaT      = 0.25*sp.pi*3.61**2/(80.0*sp.pi*3.61**2)
time           = np.arange(0, 50, cflDeltaT)#2.5*sp.pi*3.61**2/(138.8*sp.pi*3.61**2))
typeOfInput    = 'Gaussian'

typeOfSimulation = 1    # 0 for Joining, 1 for Splitting


###### SIMULATION 1 ######
print('------- Simulation 1 -------')
spaceStep      = 0.25
typeOfSolver   = 'Upwind'
typeOfSL       = 'None'
cBlood_PP      = np.zeros(len(time))
time_PP        = time.copy()
n  = 320
n2 = 320
length = 1

if typeOfSimulation:
  nbNodes, edges, nodeTypes, radius, OutFlow, Flow, Pressure = sim_initSplitting(n,n2,length)
else: 
  nbNodes, edges, nodeTypes, radius, OutFlow, Flow, Pressure = sim_initJoining(n,n2,length)

state_dX1_25_UW, nbSpaceSteps_dX1_25_UW, time_dX1_25_UW, volume_dX1_25 = Main(nbNodes, edges, nodeTypes, radius, spaceStep, OutFlow, Flow, Pressure, typeOfSolver, typeOfInput, typeOfSL, time , cBlood_PP, time_PP, cflDeltaT)


###### SIMULATION 2 ######
print('------- Simulation 2 -------')
spaceStep      = 2.5
typeOfSolver   = 'Upwind'
typeOfSL       = 'None'
cBlood_PP      = np.zeros(len(time))
time_PP        = time.copy()
n  = 32
n2 = 32
length = 1

if typeOfSimulation:
  nbNodes, edges, nodeTypes, radius, OutFlow, Flow, Pressure = sim_initSplitting(n,n2,length)
else: 
  nbNodes, edges, nodeTypes, radius, OutFlow, Flow, Pressure = sim_initJoining(n,n2,length)

state_dX2_5_UW, nbSpaceSteps_dX2_5_UW, time_dX2_5_UW, volume_dX2_5 = Main(nbNodes, edges, nodeTypes, radius, spaceStep, OutFlow, Flow, Pressure, typeOfSolver, typeOfInput, typeOfSL, time , cBlood_PP, time_PP, cflDeltaT)
typeOfSolver='SlopeLimiter'
typeOfSL = 'MC'
state_dX2_5_MC, nbSpaceSteps_dX2_5_MC, time_dX2_5_MC, volume_dX2_5 = Main(nbNodes, edges, nodeTypes, radius, spaceStep, OutFlow, Flow, Pressure, typeOfSolver, typeOfInput, typeOfSL, time , cBlood_PP, time_PP, cflDeltaT)
typeOfSolver='SlopeLimiter'
typeOfSL = 'MinMod'
state_dX2_5_MM, nbSpaceSteps_dX2_5_MM, time_dX2_5_MM, volume_dX2_5 = Main(nbNodes, edges, nodeTypes, radius, spaceStep, OutFlow, Flow, Pressure, typeOfSolver, typeOfInput, typeOfSL, time , cBlood_PP, time_PP, cflDeltaT)
typeOfSolver='SlopeLimiter'
typeOfSL = 'VanLeer'
state_dX2_5_VL, nbSpaceSteps_dX2_5_VL, time_dX2_5_VL, volume_dX2_5 = Main(nbNodes, edges, nodeTypes, radius, spaceStep, OutFlow, Flow, Pressure, typeOfSolver, typeOfInput, typeOfSL, time , cBlood_PP, time_PP, cflDeltaT)
typeOfSolver='SlopeLimiter'
typeOfSL = 'Superbee'
state_dX2_5_SB, nbSpaceSteps_dX2_5_SB, time_dX2_5_SB, volume_dX2_5 = Main(nbNodes, edges, nodeTypes, radius, spaceStep, OutFlow, Flow, Pressure, typeOfSolver, typeOfInput, typeOfSL, time , cBlood_PP, time_PP, cflDeltaT)

###### SIMULATION 3 ######
print('------- Simulation 3 -------')
spaceStep      = 5.0
typeOfSolver   = 'Upwind'
typeOfSL       = 'None'
cBlood_PP      = np.zeros(len(time))
time_PP        = time.copy()
n  = 16
n2 = 16
length = 1

if typeOfSimulation:
  nbNodes, edges, nodeTypes, radius, OutFlow, Flow, Pressure = sim_initSplitting(n,n2,length)
else: 
  nbNodes, edges, nodeTypes, radius, OutFlow, Flow, Pressure = sim_initJoining(n,n2,length)

state_dX5_UW, nbSpaceSteps_dX5_UW, time_dX5_UW, volume_dX5 = Main(nbNodes, edges, nodeTypes, radius, spaceStep, OutFlow, Flow, Pressure, typeOfSolver, typeOfInput, typeOfSL, time , cBlood_PP, time_PP, cflDeltaT)

typeOfSolver = 'SlopeLimiter'
typeOfSL = 'MC'
state_dX5_MC, nbSpaceSteps_dX5_MC, time_dX5_MC, volume_dX5 = Main(nbNodes, edges, nodeTypes, radius, spaceStep, OutFlow, Flow, Pressure, typeOfSolver, typeOfInput, typeOfSL, time , cBlood_PP, time_PP, cflDeltaT)
typeOfSolver = 'SlopeLimiter'
typeOfSL = 'MinMod'
state_dX5_MM, nbSpaceSteps_dX5_MM, time_dX5_MM, volume_dX5 = Main(nbNodes, edges, nodeTypes, radius, spaceStep, OutFlow, Flow, Pressure, typeOfSolver, typeOfInput, typeOfSL, time , cBlood_PP, time_PP, cflDeltaT)
typeOfSolver = 'SlopeLimiter'
typeOfSL = 'VanLeer'
state_dX5_VL, nbSpaceSteps_dX5_VL, time_dX5_VL, volume_dX5 = Main(nbNodes, edges, nodeTypes, radius, spaceStep, OutFlow, Flow, Pressure, typeOfSolver, typeOfInput, typeOfSL, time , cBlood_PP, time_PP, cflDeltaT)
typeOfSolver = 'SlopeLimiter'
typeOfSL = 'Superbee'
state_dX5_SB, nbSpaceSteps_dX5_SB, time_dX5_SB, volume_dX5 = Main(nbNodes, edges, nodeTypes, radius, spaceStep, OutFlow, Flow, Pressure, typeOfSolver, typeOfInput, typeOfSL, time , cBlood_PP, time_PP, cflDeltaT)


###### SIMULATION 4 ######
print('------- Simulation 4 -------')
spaceStep      = 10.0
typeOfSolver   = 'Upwind'
typeOfSL       = 'None'
cBlood_PP      = np.zeros(len(time))
time_PP        = time.copy()
n  = 8
n2 = 8
length = 1

if typeOfSimulation:
  nbNodes, edges, nodeTypes, radius, OutFlow, Flow, Pressure = sim_initSplitting(n,n2,length)
else: 
  nbNodes, edges, nodeTypes, radius, OutFlow, Flow, Pressure = sim_initJoining(n,n2,length)

state_dX10_UW, nbSpaceSteps_dX10_UW, time_dX10_UW, volume_dX10 = Main(nbNodes, edges, nodeTypes, radius, spaceStep, OutFlow, Flow, Pressure, typeOfSolver, typeOfInput, typeOfSL, time , cBlood_PP, time_PP, cflDeltaT)
typeOfSolver = 'SlopeLimiter'
typeOfSL = 'MC'
state_dX10_MC, nbSpaceSteps_dX10_MC, time_dX10_MC, volume_dX10 = Main(nbNodes, edges, nodeTypes, radius, spaceStep, OutFlow, Flow, Pressure, typeOfSolver, typeOfInput, typeOfSL, time , cBlood_PP, time_PP, cflDeltaT)
typeOfSolver = 'SlopeLimiter'
typeOfSL = 'MinMod'
state_dX10_MM, nbSpaceSteps_dX10_MM, time_dX10_MM, volume_dX10 = Main(nbNodes, edges, nodeTypes, radius, spaceStep, OutFlow, Flow, Pressure, typeOfSolver, typeOfInput, typeOfSL, time , cBlood_PP, time_PP, cflDeltaT)
typeOfSolver = 'SlopeLimiter'
typeOfSL = 'VanLeer'
state_dX10_VL, nbSpaceSteps_dX10_VL, time_dX10_VL, volume_dX10 = Main(nbNodes, edges, nodeTypes, radius, spaceStep, OutFlow, Flow, Pressure, typeOfSolver, typeOfInput, typeOfSL, time , cBlood_PP, time_PP, cflDeltaT)
typeOfSolver = 'SlopeLimiter'
typeOfSL = 'Superbee'
state_dX10_SB, nbSpaceSteps_dX10_SB, time_dX10_SB, volume_dX10 = Main(nbNodes, edges, nodeTypes, radius, spaceStep, OutFlow, Flow, Pressure, typeOfSolver, typeOfInput, typeOfSL, time , cBlood_PP, time_PP, cflDeltaT)

###### SIMULATION 5 ######
print('------- Simulation 5 -------')
spaceStep      = 20.0
typeOfSolver   = 'Upwind'
typeOfSL       = 'None'
cBlood_PP      = np.zeros(len(time))
time_PP        = time.copy()
n  = 4
n2 = 4
length = 1

if typeOfSimulation:
  nbNodes, edges, nodeTypes, radius, OutFlow, Flow, Pressure = sim_initSplitting(n,n2,length)
else: 
  nbNodes, edges, nodeTypes, radius, OutFlow, Flow, Pressure = sim_initJoining(n,n2,length)

state_dX20_UW, nbSpaceSteps_dX20_UW, time_dX20_UW, volume_dX20 = Main(nbNodes, edges, nodeTypes, radius, spaceStep, OutFlow, Flow, Pressure, typeOfSolver, typeOfInput, typeOfSL, time , cBlood_PP, time_PP, cflDeltaT)
typeOfSolver = 'SlopeLimiter'
typeOfSL = 'MC'
state_dX20_MC, nbSpaceSteps_dX20_MC, time_dX20_MC, volume_dX20 = Main(nbNodes, edges, nodeTypes, radius, spaceStep, OutFlow, Flow, Pressure, typeOfSolver, typeOfInput, typeOfSL, time , cBlood_PP, time_PP, cflDeltaT)
typeOfSolver = 'SlopeLimiter'
typeOfSL = 'MinMod'
state_dX20_MM, nbSpaceSteps_dX20_MM, time_dX20_MM, volume_dX20 = Main(nbNodes, edges, nodeTypes, radius, spaceStep, OutFlow, Flow, Pressure, typeOfSolver, typeOfInput, typeOfSL, time , cBlood_PP, time_PP, cflDeltaT)
typeOfSolver = 'SlopeLimiter'
typeOfSL = 'VanLeer'
state_dX20_VL, nbSpaceSteps_dX20_VL, time_dX20_VL, volume_dX20 = Main(nbNodes, edges, nodeTypes, radius, spaceStep, OutFlow, Flow, Pressure, typeOfSolver, typeOfInput, typeOfSL, time , cBlood_PP, time_PP, cflDeltaT)
typeOfSolver = 'SlopeLimiter'
typeOfSL = 'Superbee'
state_dX20_SB, nbSpaceSteps_dX20_SB, time_dX20_SB, volume_dX20 = Main(nbNodes, edges, nodeTypes, radius, spaceStep, OutFlow, Flow, Pressure, typeOfSolver, typeOfInput, typeOfSL, time , cBlood_PP, time_PP, cflDeltaT)


###### SIMULATION 6 ######
print('------- Simulation 6 -------')
spaceStep      = 40.0
typeOfSolver   = 'Upwind'
typeOfSL       = 'None'
cBlood_PP      = np.zeros(len(time))
time_PP        = time.copy()
n  = 2
n2 = 2
length = 1

if typeOfSimulation:
  nbNodes, edges, nodeTypes, radius, OutFlow, Flow, Pressure = sim_initSplitting(n,n2,length)
else: 
  nbNodes, edges, nodeTypes, radius, OutFlow, Flow, Pressure = sim_initJoining(n,n2,length)

state_dX40_UW, nbSpaceSteps_dX40_UW, time_dX40_UW, volume_dX40 = Main(nbNodes, edges, nodeTypes, radius, spaceStep, OutFlow, Flow, Pressure, typeOfSolver, typeOfInput, typeOfSL, time , cBlood_PP, time_PP, cflDeltaT)
typeOfSolver = 'SlopeLimiter'
typeOfSL = 'MC'
state_dX40_MC, nbSpaceSteps_dX40_MC, time_dX40_MC, volume_dX40 = Main(nbNodes, edges, nodeTypes, radius, spaceStep, OutFlow, Flow, Pressure, typeOfSolver, typeOfInput, typeOfSL, time , cBlood_PP, time_PP, cflDeltaT)
typeOfSolver = 'SlopeLimiter'
typeOfSL = 'MinMod'
state_dX40_MM, nbSpaceSteps_dX40_MM, time_dX40_MM, volume_dX40 = Main(nbNodes, edges, nodeTypes, radius, spaceStep, OutFlow, Flow, Pressure, typeOfSolver, typeOfInput, typeOfSL, time , cBlood_PP, time_PP, cflDeltaT)
typeOfSolver = 'SlopeLimiter'
typeOfSL = 'VanLeer'
state_dX40_VL, nbSpaceSteps_dX40_VL, time_dX40_VL, volume_dX40 = Main(nbNodes, edges, nodeTypes, radius, spaceStep, OutFlow, Flow, Pressure, typeOfSolver, typeOfInput, typeOfSL, time , cBlood_PP, time_PP, cflDeltaT)
typeOfSolver = 'SlopeLimiter'
typeOfSL = 'Superbee'
state_dX40_SB, nbSpaceSteps_dX40_SB, time_dX40_SB, volume_dX40 = Main(nbNodes, edges, nodeTypes, radius, spaceStep, OutFlow, Flow, Pressure, typeOfSolver, typeOfInput, typeOfSL, time , cBlood_PP, time_PP, cflDeltaT)


###### POSTPROCESSING ######
print('------- Postprocessing -------')
if typeOfSimulation:
  idx = (int)(-1)
else:
  idx = (int)(0)

L2_40_UW = sp.linalg.norm(state_dX40_UW[idx,:]/volume_dX40[idx] - state_dX1_25_UW[idx,:]/volume_dX1_25[idx],2)
L2_40_MC = sp.linalg.norm(state_dX40_MC[idx,:]/volume_dX40[idx] - state_dX1_25_UW[idx,:]/volume_dX1_25[idx],2)
L2_40_VL = sp.linalg.norm(state_dX40_VL[idx,:]/volume_dX40[idx] - state_dX1_25_UW[idx,:]/volume_dX1_25[idx],2)
L2_40_SB = sp.linalg.norm(state_dX40_SB[idx,:]/volume_dX40[idx] - state_dX1_25_UW[idx,:]/volume_dX1_25[idx],2)
L2_40_MM = sp.linalg.norm(state_dX40_MM[idx,:]/volume_dX40[idx] - state_dX1_25_UW[idx,:]/volume_dX1_25[idx],2)


L2_20_UW = sp.linalg.norm(state_dX20_UW[idx,:]/volume_dX20[idx] - state_dX1_25_UW[idx,:]/volume_dX1_25[idx],2)
L2_20_MC = sp.linalg.norm(state_dX20_MC[idx,:]/volume_dX20[idx] - state_dX1_25_UW[idx,:]/volume_dX1_25[idx],2)
L2_20_VL = sp.linalg.norm(state_dX20_VL[idx,:]/volume_dX20[idx] - state_dX1_25_UW[idx,:]/volume_dX1_25[idx],2)
L2_20_SB = sp.linalg.norm(state_dX20_SB[idx,:]/volume_dX20[idx] - state_dX1_25_UW[idx,:]/volume_dX1_25[idx],2)
L2_20_MM = sp.linalg.norm(state_dX20_MM[idx,:]/volume_dX20[idx] - state_dX1_25_UW[idx,:]/volume_dX1_25[idx],2)

L2_10_UW = sp.linalg.norm(state_dX10_UW[idx,:]/volume_dX10[idx] - state_dX1_25_UW[idx,:]/volume_dX1_25[idx],2)
L2_10_MC = sp.linalg.norm(state_dX10_MC[idx,:]/volume_dX10[idx] - state_dX1_25_UW[idx,:]/volume_dX1_25[idx],2)
L2_10_VL = sp.linalg.norm(state_dX10_VL[idx,:]/volume_dX10[idx] - state_dX1_25_UW[idx,:]/volume_dX1_25[idx],2)
L2_10_SB = sp.linalg.norm(state_dX10_SB[idx,:]/volume_dX10[idx] - state_dX1_25_UW[idx,:]/volume_dX1_25[idx],2)
L2_10_MM = sp.linalg.norm(state_dX10_MM[idx,:]/volume_dX10[idx] - state_dX1_25_UW[idx,:]/volume_dX1_25[idx],2)

L2_5_UW = sp.linalg.norm(state_dX5_UW[idx,:]/volume_dX5[idx] - state_dX1_25_UW[idx,:]/volume_dX1_25[idx],2)
L2_5_MC = sp.linalg.norm(state_dX5_MC[idx,:]/volume_dX5[idx] - state_dX1_25_UW[idx,:]/volume_dX1_25[idx],2)
L2_5_VL = sp.linalg.norm(state_dX5_VL[idx,:]/volume_dX5[idx] - state_dX1_25_UW[idx,:]/volume_dX1_25[idx],2)
L2_5_SB = sp.linalg.norm(state_dX5_SB[idx,:]/volume_dX5[idx] - state_dX1_25_UW[idx,:]/volume_dX1_25[idx],2)
L2_5_MM = sp.linalg.norm(state_dX5_MM[idx,:]/volume_dX5[idx] - state_dX1_25_UW[idx,:]/volume_dX1_25[idx],2)

L2_2_5_UW = sp.linalg.norm(state_dX2_5_UW[idx,:]/volume_dX2_5[idx] - state_dX1_25_UW[idx,:]/volume_dX1_25[idx],2)
L2_2_5_MC = sp.linalg.norm(state_dX2_5_MC[idx,:]/volume_dX2_5[idx] - state_dX1_25_UW[idx,:]/volume_dX1_25[idx],2)
L2_2_5_VL = sp.linalg.norm(state_dX2_5_VL[idx,:]/volume_dX2_5[idx] - state_dX1_25_UW[idx,:]/volume_dX1_25[idx],2)
L2_2_5_SB = sp.linalg.norm(state_dX2_5_SB[idx,:]/volume_dX2_5[idx] - state_dX1_25_UW[idx,:]/volume_dX1_25[idx],2)
L2_2_5_MM = sp.linalg.norm(state_dX2_5_MM[idx,:]/volume_dX2_5[idx] - state_dX1_25_UW[idx,:]/volume_dX1_25[idx],2)

L2_analytic = sp.linalg.norm(state_dX1_25_UW[idx,:]/volume_dX1_25[idx])

L2_40_UW_rel = L2_40_UW/L2_analytic
L2_40_MC_rel = L2_40_MC/L2_analytic
L2_40_MM_rel = L2_40_MM/L2_analytic
L2_40_VL_rel = L2_40_VL/L2_analytic
L2_40_SB_rel = L2_40_SB/L2_analytic

L2_20_UW_rel = L2_20_UW/L2_analytic
L2_20_MC_rel = L2_20_MC/L2_analytic
L2_20_MM_rel = L2_20_MM/L2_analytic
L2_20_VL_rel = L2_20_VL/L2_analytic
L2_20_SB_rel = L2_20_SB/L2_analytic

L2_10_UW_rel = L2_10_UW/L2_analytic
L2_10_MC_rel = L2_10_MC/L2_analytic
L2_10_MM_rel = L2_10_MM/L2_analytic
L2_10_VL_rel = L2_10_VL/L2_analytic
L2_10_SB_rel = L2_10_SB/L2_analytic

L2_5_UW_rel = L2_5_UW/L2_analytic
L2_5_MC_rel = L2_5_MC/L2_analytic
L2_5_MM_rel = L2_5_MM/L2_analytic
L2_5_VL_rel = L2_5_VL/L2_analytic
L2_5_SB_rel = L2_5_SB/L2_analytic

L2_2_5_UW_rel = L2_2_5_UW/L2_analytic
L2_2_5_MC_rel = L2_2_5_MC/L2_analytic
L2_2_5_MM_rel = L2_2_5_MM/L2_analytic
L2_2_5_VL_rel = L2_2_5_VL/L2_analytic
L2_2_5_SB_rel = L2_2_5_SB/L2_analytic

###### PLOTTING ######
plt.figure()

dX = [2.5, 5.0, 10, 20, 40]
dUW = [L2_2_5_UW_rel, L2_5_UW_rel, L2_10_UW_rel, L2_20_UW_rel, L2_40_UW_rel]
dMC = [L2_2_5_MC_rel, L2_5_MC_rel, L2_10_MC_rel, L2_20_MC_rel, L2_40_MC_rel]
dMM = [L2_2_5_MM_rel, L2_5_MM_rel, L2_10_MM_rel, L2_20_MM_rel, L2_40_MM_rel]
dVL = [L2_2_5_VL_rel, L2_5_VL_rel, L2_10_VL_rel, L2_20_VL_rel, L2_40_VL_rel]
dSB = [L2_2_5_SB_rel, L2_5_SB_rel, L2_10_SB_rel, L2_20_SB_rel, L2_40_SB_rel]
print(dUW)
print(dMC)
print(dMM)
print(dVL)
print(dSB)

plt.plot(dX, dSB, 'r.', label='Superbee')
plt.plot(dX, dMC, 'c.', label='MC')
plt.plot(dX, dMM, 'y.', label='MinMod')
plt.plot(dX, dVL, 'b.', label='VanLeer')
plt.plot(dX, dUW, 'k.', label='Upwind')

plt.yscale('log')
plt.xscale('log')
plt.ylabel(r'$L_{2}$ error', fontsize=14)
plt.xlabel(r'$\Delta x$ in $\mu m$', fontsize=14)
plt.legend(loc='best', fancybox='round', frameon = True)


#plt.xlim(2.5, 90.0)
if typeOfSimulation:
  plt.savefig('Figure_Accuracy_Splitting.pdf', dpi=400)
else: 
  plt.savefig('Figure_Accuracy_Joining.pdf', dpi=400)


plt.show()
