'''
Numerical scheme implementation


'''
import scipy as sp
import numpy as np
# import matplotlib.pyplot as plt
# import pandas as pd
from scipy import interpolate
class Convection_computation:
    def __init__(self, nbNodes, edges, nodeTypes, radius, spaceStep, OutFlow, flow, Pressure, typeOfSolver, typeOfInput, typeOfSL, tMax, CFLDeltaT, cBlood_PP, time_PP):
        self.nbSpaceSteps = nbNodes
        self.radius = radius.copy()
        self.dX = spaceStep
        self.OutFlow = OutFlow.copy()
        self.Flow = flow.copy()
        self.nodeTypes = nodeTypes.copy()
        self.edges = edges.copy()

        self.Pressure = Pressure.copy()
        self.nbEdgesConnected = sp.zeros(self.nbSpaceSteps)
        for i in range(len(self.edges)):
            self.nbEdgesConnected[self.edges[i][0]] = self.nbEdgesConnected[self.edges[i][0]] + 1
            self.nbEdgesConnected[self.edges[i][1]] = self.nbEdgesConnected[self.edges[i][1]] + 1

        maxedges = (int)(max(self.nbEdgesConnected))
        self.edgesConnectedToNode = np.ones((self.nbSpaceSteps,maxedges), dtype=int)*(-1)
        for i in range(len(self.edges)):
            v = 0
            while(v <= maxedges and self.edgesConnectedToNode[edges[i][0]][v]!=-1):
                v = v+1
            self.edgesConnectedToNode[edges[i][0]][v] = i
            v = 0
            while(v <= maxedges and self.edgesConnectedToNode[edges[i][1]][v]!=-1):
                v = v+1
            self.edgesConnectedToNode[edges[i][1]][v] = i

        self.Volume = sp.zeros(self.nbSpaceSteps)
        for i in range(self.nbSpaceSteps):
            self.Volume[i] = self.getVolumeOfANode(i)
        #print(self.OutFlow, self.Volume, self.nbEdgesConnected)
        #print(self.Volume/self.OutFlow)
        self.CFLdeltaT = min(self.Volume/self.OutFlow)

        if(typeOfSolver == 'UpwindCFL1'):
            self.CFLdeltaT = CFLDeltaT
            for i in range(self.nbSpaceSteps):
                self.Volume[i] = self.CFLdeltaT*self.OutFlow[i]

        #print(self.CFLdeltaT)

        self.insideConcentration  = 0
        self.outsideConcentration = 0

        self.typeOfSolver = typeOfSolver
        self.typeOfSL = typeOfSL
        self.typeOfInput = typeOfInput
        self.tMax = tMax
        #print(time_PP, cBlood_PP)
        self.cBlood = interpolate.interp1d(time_PP, cBlood_PP)
        self.tmaxInt = tMax
        self.k = 2
        self.epsilon = 1*10**(-6)
        self.d = sp.zeros(2)
        self.d[0] = 2./3
        self.d[1] = 1./3

        self.errorREL = []
    def getVolumeOfANode(self, nodeIndex):
        volume = 0
        max_Edge_Radius = 0
        volume_Cylinders = 0;
        v = 0
        while(v<len(self.edgesConnectedToNode[nodeIndex]) and self.edgesConnectedToNode[nodeIndex][v]!=-1):
            volumeOfEdge = self.radius[self.edgesConnectedToNode[nodeIndex][v]]**2*sp.pi*self.dX
            volume_Cylinders = volume_Cylinders + volumeOfEdge*0.5
            if(max_Edge_Radius < self.radius[self.edgesConnectedToNode[nodeIndex][v]]):
                max_Edge_Radius = self.radius[self.edgesConnectedToNode[nodeIndex][v]]
            v = v+1

        if(self.nodeTypes[nodeIndex]==2 or self.nodeTypes[nodeIndex]==1):
            volume_Cylinders = self.radius[self.edgesConnectedToNode[nodeIndex][0]]**2*sp.pi*self.dX
        volume_Intersection=0
        '''
        if(self.nbEdgesConnected[nodeIndex]>2):
            min_length = self.dX
            if(min_length > max_Edge_Radius):
                v = 0
                while(v<len(self.edgesConnectedToNode[nodeIndex]) and self.edgesConnectedToNode[nodeIndex][v]!=-1):
                    if(self.radius[self.edgesConnectedToNode[nodeIndex][v]] >= max_Edge_Radius):
                        volume_Intersection = volume_Intersection + 2.*sp.pi/3. * max_Edge_Radius**3
                    else:
                        h = sp.sqrt(max_Edge_Radius**2 - self.radius[self.edgesConnectedToNode[nodeIndex][v]]**2)
                        volume_Intersection = volume_Intersection + 2*sp.pi/3.*(max_Edge_Radius-h)**2*(2*max_Edge_Radius + h)
                    v = v+1
                volume = volume_Cylinders -  volume_Intersection + 4/3.*sp.pi*max_Edge_Radius**3
            else:
                v = 0
                while(v<len(self.edgesConnectedToNode[nodeIndex]) and self.edgesConnectedToNode[nodeIndex][v]!=-1):
                    if(self.radius[self.edgesConnectedToNode[nodeIndex][v]] >= min_length/2.):
                        volume_Intersection = volume_Intersection + 2*sp.pi/3. * (min_length/2.)**3
                    else:
                        h = sp.sqrt((min_length/2.)**2 - (self.radius[self.edgesConnectedToNode[nodeIndex][v]])**2)
                        volume_Intersection = volume_Intersection + sp.pi/3. * (min_length/2. - h)**2*(2. * min_length/2 + h)
                    v = v+1
                volume = volume_Cylinders -  volume_Intersection + 4/3.*sp.pi*pow(min_length/2.,3)
        else:
        '''
        volume = volume_Cylinders
        return volume

    def PropageOneTimePoint(self, state, t0, t):
        U                   = sp.zeros(self.nbSpaceSteps)

        nbtimeSteps         = 1
        for i in range(self.nbSpaceSteps):
            U[i]            = state[i]

        deltaT              = (t - t0)/nbtimeSteps
        #print(self.CFLdeltaT, (deltaT-self.CFLdeltaT))

        while((deltaT - self.CFLdeltaT)> 1e-8 and abs(deltaT-self.CFLdeltaT)>1e-8):
            #print ('ERROR ∆t too high, ∆t will now be set at:', deltaT/2., t0, t, deltaT, self.CFLdeltaT, deltaT-self.CFLdeltaT)
            deltaT          = deltaT/2.
            nbtimeSteps     = 2 * nbtimeSteps
            print('delta T too high \n', deltaT, self.CFLdeltaT, (deltaT-self.CFLdeltaT))
        #print(deltaT, self.CFLdeltaT)
        self.errorREL.append((self.CFLdeltaT - deltaT)/self.CFLdeltaT)
        inPlus  = 0
        inMinus = 0
        for n in range(int(nbtimeSteps)):
            U, InsideMinus, InsidePlus, OutsideConcentration, InsideConcentration = self.RunOneTimeStep(deltaT, U, t0 + n*deltaT)
            inPlus                    = inPlus + InsidePlus
            inMinus                   = inMinus + InsideMinus
            self.insideConcentration  = self.insideConcentration + InsideConcentration
            self.outsideConcentration = self.outsideConcentration + OutsideConcentration
            #print('U', U)
            if( abs(inPlus-inMinus)>10**(-7) ):
                print ('t0:', t0, 'inside plus', inPlus, 'inside Minus', inMinus, 'delta', inPlus-inMinus)
            #wait = input("PRESS ENTER TO CONTINUE.")
        outState = state.copy()
        for i in range(self.nbSpaceSteps):
            outState[i] = U[i]

        return outState

    ## Input functions
    def Creneau(self, t):
        '''Square impulse function'''
        result = 0
        if(t/self.tMax > 0.15 and t/self.tMax < 0.17):
            result =  10.
        return result

    def Gaussian(self, t):
        '''Gaussian impulse function'''
        return  120*(1/(0.075*20*sp.sqrt(2.*sp.pi))*sp.exp(-(t-0.30*20.)**2/(2*(0.075*20.)**2)))#(100/(40.*sp.sqrt(2.*sp.pi))*sp.exp(-(t-100)**2/(2*40**2)))

    def AIF(self, t):
        '''Arterial input function'''
        A1 = 0.809 * 60*10**3*10**(-15)
        A2 = 0.330 * 60*10**3*10**(-15)
        T1 = 0.17046 * 60
        T2 = 0.365 * 60
        sigma1 = 0.056 * 60
        sigma2 = 0.132 * 60
        alpha = 1.050*10**3*10**(-15)
        beta = 0.169 / 60
        s = 38.078 / 60
        tau = 0.483 * 60
        c1 = A1/(sigma1*sp.sqrt(2*sp.pi))*sp.exp(-(t-T1)**2/((2.*sigma1)**2))
        c2 = A2/(sigma2*sp.sqrt(2*sp.pi))*sp.exp(-(t-T2)**2/((2.*sigma2)**2))
        return c1 + c2 + alpha*sp.exp(-beta*t)/(1+sp.exp(-s*(t-tau)))

    def CLFInput(self, t):
        '''CLF Input data. TODO: experimental?'''
        if(t>=self.tmaxInt):
            return 0
        else:
            return self.cBlood(t)

    ## Flux limiters
    def MC(self, r):
        return max(0, min(min((1 + r)/2.,2.),2.*r))

    def VanLeer(self, r):
        return (r + abs(r))/(1 + abs(r))

    def Superbee(self, r):
        return max(0, max(min(1, 2*r), min(2, r)))

    def minmod(self, a, b):
        result = 0
        if(abs(a) < abs(b) and a*b > 0):
            result = a
        if( abs(b) > abs(a) and a*b > 0):
            result = b
        return 0

    def min_mod(self, r):
        return max(0, min(1,r))#self.minmod(1, r)

    def LaxWendroff(self, r):
        return 1

    def go(self, t):
        '''Set input concentration profile.'''
        result = self.Gaussian(t)
        if(self.typeOfInput == 'Creneau'):
            result = self.Creneau(t)
        else:
            if(self.typeOfInput == 'AIF'):
                result = self.AIF(t)
            else:
                if(self.typeOfInput == 'CLFInput'):
                    result = self.CLFInput(t)
        return result

    def Phi(self, r):
        '''Set flux limiter.'''
        result = self.MC(r)
        if(self.typeOfSL == 'VanLeer'):
            result = self.VanLeer(r)
        else:
            if(self.typeOfSL == 'MinMod'):
                result = self.min_mod(r)
            else:
                if(self.typeOfSL == 'Superbee'):
                    result = self.Superbee(r)
        return result


    def getAlpha(self, r, beta):
        return self.d[r]/(self.epsilon + beta)**2

    def getBeta(self, r, U, UPlus, UMoins):
        if(self.r == 0):
            return (UPlus - U)**2
        if(self.r == 1):
            return (U - UMoins)**2

    def getSumAlpha(self, U, UPlus, UMoins):
        sumAlpha     = 0
        for r in range(self.k):
            beta     = self.getBeta(r, U, UPlus, UMoins)
            alpha_r  = self.getAlpha(r, beta)
            sumAlpha = sumAlpha + alpha_r
        return sumAlpha

    def getOmega(self, r, U, UPlus, UMoins):
        beta      = self.getBeta(r, U, UPlus, UMoins)
        alpha_r   = self.getAlpha(r, beta)
        sum_alpha = self.getSumAlpha(U, UPlus, UMoins)
        return alpha_r/sum_alpha

    def WENO_UMoins(self, U, UPlus, UMoins):
        U_0 = 1/2.*U + 1/2.*UPlus
        U_1 = -1/2.*UMoins + 3/2.*U
        w_0 = self.getOmega(0, U, UPlus, UMoins)
        w_1 = self.getOmega(1, U, UPlus, UMoins)
        return w_0 * U_0 + w_1 * U_1



    def RunOneTimeStep(self, DeltaT, U, t):
        '''Implementation of numerical schemes detailed in Boissier et al.'''
        dU = sp.zeros(self.nbSpaceSteps) # dU = U^{n+1} - U^n

        # TODO figure out what these mean
        inside_plus = 0
        inside_minus = 0
        outsideConcentration = 0
        insideConcentration  = 0
        if(self.typeOfSolver == 'Upwind' or self.typeOfSolver == 'UpwindCFL1'):
            for i in range(self.nbSpaceSteps):
                if(self.nodeTypes[i] == 2): # inlet nodes -> Ghost cells
                    U_minus1 = self.go(t + self.Volume[i]/(self.OutFlow[i]*2.))
                    dU[i] = U[i] + DeltaT*self.OutFlow[i]*U_minus1 - DeltaT*self.OutFlow[i] * U[i]/self.Volume[i]
                    inside_minus = inside_minus + DeltaT*self.OutFlow[i]*U[i]/self.Volume[i]
                    insideConcentration = insideConcentration + DeltaT*self.OutFlow[i]*U_minus1
                else:
                    v = 0
                    inflowNeighbor = []
                    while(v < len(self.edgesConnectedToNode[i]) and self.edgesConnectedToNode[i][v]!=-1):
                        ntc = self.edges[self.edgesConnectedToNode[i][v]][0]
                        if(ntc == i):
                            ntc = self.edges[self.edgesConnectedToNode[i][v]][1]
                        if(self.Pressure[ntc] > self.Pressure[i]):
                            toAppend = sp.zeros(3)
                            toAppend[0] = self.Volume[ntc]
                            toAppend[1] = abs(self.Flow[self.edgesConnectedToNode[i][v]])
                            toAppend[2] = U[ntc]
                            inflowNeighbor.append(toAppend)
                        v = v+1
                    inflow_i = 0
                    for k in range(len(inflowNeighbor)):
                        inflow_i += inflowNeighbor[k][1]/inflowNeighbor[k][0]*inflowNeighbor[k][2]

                    dU[i] = U[i] + DeltaT*inflow_i - DeltaT*self.OutFlow[i]*U[i]/self.Volume[i]
                    inside_plus = inside_plus + inflow_i*DeltaT
                    if(self.nodeTypes[i] == 1):
                        outsideConcentration = outsideConcentration + DeltaT*self.OutFlow[i]*U[i]/self.Volume[i]
                    else:
                        inside_minus = inside_minus + DeltaT*self.OutFlow[i]*U[i]/self.Volume[i]
        if(self.typeOfSolver == 'SlopeLimiter'):
            for i in range(self.nbSpaceSteps):
                inflowNeighbor = []
                outflowNeighbor = []
                secondInflowNeighbor = []
                v = 0
                F_iplus = 0
                F_iminus = 0
                while(v<len(self.edgesConnectedToNode[i]) and self.edgesConnectedToNode[i][v] != -1):
                    ntc = self.edges[self.edgesConnectedToNode[i][v]][0]
                    if(ntc == i):
                        ntc = self.edges[self.edgesConnectedToNode[i][v]][1]

                    if(self.Pressure[ntc] > self.Pressure[i]):
                        toAppend = sp.zeros(4)
                        toAppend[0] = self.Volume[ntc]
                        toAppend[1] = abs(self.Flow[self.edgesConnectedToNode[i][v]])
                        toAppend[2] = U[ntc]
                        toAppend[3] = ntc
                        inflowNeighbor.append(toAppend)
                        p = 0
                        while(p < len(self.edgesConnectedToNode[ntc]) and self.edgesConnectedToNode[ntc][p]!=-1):
                            sntc = self.edges[self.edgesConnectedToNode[ntc][p]][0]
                            if(sntc == ntc):
                                sntc = self.edges[self.edgesConnectedToNode[ntc][p]][1]
                            if(self.Pressure[sntc]>self.Pressure[ntc]):
                                toAppend = sp.zeros(4)
                                toAppend[0] = self.Volume[sntc]
                                toAppend[1] = abs(self.Flow[self.edgesConnectedToNode[ntc][p]])
                                toAppend[2] = U[sntc]
                                toAppend[3] = ntc
                                secondInflowNeighbor.append(toAppend)
                            p = p+1
                    if(self.Pressure[i]>self.Pressure[ntc]):
                        toAppend = sp.zeros(3)
                        toAppend[0] = self.Volume[ntc]
                        toAppend[1] = abs(self.Flow[self.edgesConnectedToNode[i][v]])

                        toAppend[2] = U[ntc]
                        outflowNeighbor.append(toAppend)
                    v = v+1
                if(self.nodeTypes[i] == 2): # inlet nodes
                    U_minus1 = self.go(t + self.Volume[i]/(self.OutFlow[i]*2.))
                    U_minus2 = self.go(t + self.Volume[i]*3./(self.OutFlow[i]*2.))
                    F_iminus = self.OutFlow[i] * U_minus1*DeltaT
                    F_iplus  = self.OutFlow[i] * U[i]/self.Volume[i] * DeltaT
                    if(abs(U[i]/self.Volume[i]-U_minus1)>0):
                        F_iminus = F_iminus + DeltaT*1./2 * self.OutFlow[i]* (1-self.OutFlow[i]*DeltaT/self.Volume[i]) * (U[i]/self.Volume[i]-U_minus1) * self.Phi((U_minus1-U_minus2)/(U[i]/self.Volume[i]-U_minus1))

                    for p in range(len(outflowNeighbor)):
                        if(abs(outflowNeighbor[p][2]/outflowNeighbor[p][0] - U[i]/self.Volume[i])>0):
                            F_iplus = F_iplus +  DeltaT*1./2 * self.OutFlow[i] * (1-self.OutFlow[i]*DeltaT/self.Volume[i]) * (outflowNeighbor[p][2]/outflowNeighbor[p][0] - U[i]/self.Volume[i]) * self.Phi((U[i]/self.Volume[i]-U_minus1)/(outflowNeighbor[p][2]/outflowNeighbor[p][0] - U[i]/self.Volume[i]))
                    
                    dU[i] = U[i] + F_iminus - F_iplus # Eq. (18) from Boissier et al., dU = U^{n+1} - U^n
                    inside_minus = inside_minus + F_iplus
                    insideConcentration = insideConcentration + F_iminus
                else:
                    for k in range(len(inflowNeighbor)):
                        F_iminus = F_iminus + inflowNeighbor[k][2]/inflowNeighbor[k][0] * inflowNeighbor[k][1] * DeltaT
                        if(abs(inflowNeighbor[k][2]/inflowNeighbor[k][0] - U[i]/self.Volume[i])>0):
                            if(self.nodeTypes[(int)(inflowNeighbor[k][3])] == 2):
                                U_minus1 = self.go(t + inflowNeighbor[k][0]/(self.OutFlow[(int)(inflowNeighbor[k][3])]*2.))
                                r = (inflowNeighbor[k][2]/inflowNeighbor[k][0] - U_minus1)/(U[i]/self.Volume[i] - inflowNeighbor[k][2]/inflowNeighbor[k][0])
                                F_iminus = F_iminus + 0.5*DeltaT*inflowNeighbor[k][1]*(1-DeltaT*inflowNeighbor[k][1]/inflowNeighbor[k][0])*self.Phi(r)*(U[i]/self.Volume[i]-inflowNeighbor[k][2]/inflowNeighbor[k][0])
                                #print(i,'1_minus',k, r,'phi:', self.Phi(r))
                            else:
                                for p in range(len(secondInflowNeighbor)):
                                    if(secondInflowNeighbor[p][3] == inflowNeighbor[k][3]):
                                        r = (inflowNeighbor[k][2]/inflowNeighbor[k][0] - secondInflowNeighbor[p][2]/secondInflowNeighbor[p][0])/(U[i]/self.Volume[i]-inflowNeighbor[k][2]/inflowNeighbor[k][0])
                                        F_iminus = F_iminus + DeltaT*0.5*secondInflowNeighbor[p][1]/self.OutFlow[(int)(inflowNeighbor[k][3])]*inflowNeighbor[k][1]*(1-DeltaT*inflowNeighbor[k][1]/inflowNeighbor[k][0])*self.Phi(r)*(U[i]/self.Volume[i]-inflowNeighbor[k][2]/inflowNeighbor[k][0])


                    F_iplus = F_iplus + U[i]/self.Volume[i] * DeltaT*self.OutFlow[i]
                    for k in range(len(outflowNeighbor)):
                        for p in range(len(inflowNeighbor)):
                            if(abs(outflowNeighbor[k][2]/outflowNeighbor[k][0] - U[i]/self.Volume[i])>0):
                                r = (U[i]/self.Volume[i]-inflowNeighbor[p][2]/inflowNeighbor[p][0])/(outflowNeighbor[k][2]/outflowNeighbor[k][0]-U[i]/self.Volume[i])
                                F_iplus = F_iplus + DeltaT*0.5*inflowNeighbor[p][1]/self.OutFlow[i]*outflowNeighbor[k][1]*(1-DeltaT*outflowNeighbor[k][1]/self.Volume[i])*self.Phi(r)*(outflowNeighbor[k][2]/outflowNeighbor[k][0] - U[i]/self.Volume[i])
                                
                    dU[i] = U[i] + F_iminus - F_iplus # Eq. (18) from Boissier et al., dU = U^{n+1} - U^n
                    inside_plus = inside_plus + F_iminus
                    if(self.nodeTypes[i] == 1):
                        outsideConcentration = outsideConcentration + F_iplus
                    else:
                        inside_minus = inside_minus + F_iplus
                    
        return dU, inside_minus, inside_plus, outsideConcentration, insideConcentration
